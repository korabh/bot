# Bot

Bot::Factory  ` Prototype
   ` ~ Assembly
   ` Press
   ` Delivery 
   ` ~ Builder

   ` ~ Actions
   ` ~ Draw
   ` Composite

  Line: 1. Fetch from given source.
  Line: 2. Choose prototype for given prayer time.
  Line: 3. Build it.
  Line: 4. Ground all parts in an assembly.
  Line: 5. Post to given adapters from /weebo.
  Line: 6. It should have analytics for all posted images.

  Base Module:

  class Example < Bot::Base

  class Assembly < Bot::Factory
    class Crop 
      def missing_method(m, *args, &block)
        # ..
      end

    end
  end
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bot'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bot

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake false` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/bot.

