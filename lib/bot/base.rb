module Bot
  class Base
    attr_reader :file

    def initialize(file) 
      # ...
    end

    def self.source
      # ...
    end

    def resources
      ['tanzil.net/trans', 'docs.globalquran.com']
    end

    private

    def self.read 
      File.read(File.expand_path('path'))
    end
  end
end
